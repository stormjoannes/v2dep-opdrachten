module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (head:tail) = head + ex1 tail
-- ^Hier tel ik de hele lijst bij elkaar op door steeds de head op te tellen bij de tail.

-- Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex2 :: [Int] -> [Int]
ex2 [] =[]
ex2 (head:tail) = head+1 : ex2(tail)
-- ^Hier maak ik een lijst en zet hier de head + 1 in. ex2 van de tail append ik vervolgens weer aan deze lijst.

-- Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (head:tail) = [head*(-1)] ++ ex3(tail)
-- ^Hier vermenigvuldig ik ieder element van de lijst met -1. ik append vervolgens de tail.

-- Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] resultList = resultList
ex4 (head:tail) resultList = head : tail ++ resultList
-- ^Hier plak ik de lijsten aan elkaar door de head en de tail van lijst 1 te appenden aan lijst2.

-- Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex5 :: [Int] -> [Int] -> [Int]
ex5 [][] = []
ex5 (head:tail)(head2:tail2) = [(head + head2)] ++ ex5(tail)(tail2)
-- ^Hier som ik paarsgewijs de lijsten bij elkaar op door steeds de heads van beide lijsten bij elkaar op te sommen.

-- Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
ex6 :: [Int] -> [Int] -> [Int]
ex6 [][] = []
ex6 (head:tail)(head2:tail2) = [(head * head2)] ++ ex6(tail)(tail2)
-- ^Hier vermenigvuldig ik paarsgewijs de lijsten bij elkaar op door steeds de heads van beide lijsten te vermenigvuldigen met elkaar. 

-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig product uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
ex7 :: [Int] -> [Int] -> Int
ex7 [] [] = 0
ex7 (head:tail) (head2:tail2) = ex1 (ex6(head:tail) (head2:tail2))
{- ^Hier vermenigvuldig ik paarsgewijs de lijsten bij elkaar op door steeds de heads van beide lijsten te vermenigvuldigen met elkaar, door middel van ex6.
vervolgens gebruik ik ex1 om al deze producten in de lijst bij elkaar op te tellen.
-}
 